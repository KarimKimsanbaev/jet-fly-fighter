﻿using UnityEngine;

public class SpawnTileTriggerCntroller : MonoBehaviour
{
    public Transform SpawnPoint;
    public GameObject SpawnTileObject;

    // Start is called before the first frame update
    void Start ()
    {

    }

    // Update is called once per frame
    void Update ()
    {

    }

    void OnTriggerEnter (Collider other)
    {
        if (other.tag == "Controll")
        {
            Instantiate (SpawnTileObject, SpawnPoint.position, Quaternion.identity);
            Debug.Log ("Spawn new Tile");
        }
    }
}