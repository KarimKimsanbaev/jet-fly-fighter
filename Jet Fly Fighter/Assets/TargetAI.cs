﻿using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets
{
    public class TargetAI : MonoBehaviour
    {
        public Transform targetMarker;
        public Transform destinationNavMesh;
        public NavMeshSurface nav;

        public NavMeshAgent[] navAgents;

        public GameObject waypointTarget;

        public GameObject gameIsNotRun;

        private void Start ()
        {
            //nav.BuildNavMesh ();
            navAgents = FindObjectsOfType (typeof (NavMeshAgent)) as NavMeshAgent[];
            //UpdateTargets (destinationNavMesh.position);

            gameIsNotRun = GameObject.Find("GameIsNotRun");
        }

        public void UpdateTargets (Vector3 targetPosition)
        {
            navAgents = FindObjectsOfType (typeof (NavMeshAgent)) as NavMeshAgent[];
            foreach (NavMeshAgent agent in navAgents)
            {
                agent.destination = targetPosition;
            }
            waypointTarget.transform.position = targetPosition;
        }

        public float yOffset = 20f;

        public float startTime = 0f;

        private void Update ()
        {
            if(gameIsNotRun.activeInHierarchy)
            {
                return;
            }

            startTime += Time.deltaTime;
            if (startTime < 10)
            {
                if (startTime < 5)
                {
                    Time.timeScale = 5f;
                    return;
                }
                else
                {
                    Time.timeScale = 2f;
                    return;
                }
            }
            else
            {
                if (!PauseMenuBehaviour.paused)
                {
                    Time.timeScale = 1;
                }
            }

            //if(destinationNavMesh)
            if (GetInput () && false)
            {
                Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
                RaycastHit hitInfo;

                // if (Physics.Raycast (ray.origin, ray.direction, out hitInfo))
                if (Physics.Raycast (transform.position, ray.direction, out hitInfo))
                {
                    Vector3 targetPosition = hitInfo.point;
                    // UpdateTargets (targetPosition);
                    targetMarker.position = targetPosition;
                }

            }

            var h = CrossPlatformInputManager.GetAxis ("Horizontal");
            var v = CrossPlatformInputManager.GetAxis ("Vertical");

            if (h != 0 || v != 0)
            {
                var pos = targetMarker.position;
                targetMarker.localPosition = new Vector3 (h * 1000, v * 1000, 1000);

                //Debug.Log ($"X: {h} Y: {v}");
            }
            else
            {
                var x = Mathf.Clamp (targetMarker.localPosition.x, -100, 100);
                targetMarker.localPosition = new Vector3 (x,
                    yOffset, 1000);
            }
        }

        private bool GetInput ()
        {
            if (Input.GetMouseButton (0))
            {
                return true;
            }
            return false;
        }

        private void OnDrawGizmos ()
        {
            Debug.DrawLine (targetMarker.position, targetMarker.position + Vector3.up * 5, Color.red);
        }
    }
}