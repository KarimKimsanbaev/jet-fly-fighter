﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aerop : MonoBehaviour
{
    // Start is called before the first frame update
    void Start ()
    {

    }

    public GameObject object2;

    // Update is called once per frame
    void Update ()
    {
        Vector3 p;
        if (object2 != null)
        {
            p = object2.transform.position;
        }
        else
        {
            p = new Vector3 (0, 0, 0);
        }
        gameObject.transform.position = new Vector3 (p.x, 0, p.z);
    }
}