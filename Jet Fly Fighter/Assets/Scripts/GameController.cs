﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; // List
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityStandardAssets;
/// <summary>
/// Controls the main gameplay
/// </summary>
public class GameController : MonoBehaviour
{
    [Tooltip ("A reference to the tile we want to spawn")]
    public Transform tile;
    [Tooltip ("A reference to the obstacle we want to spawn")]
    public Transform obstacle;
    [Tooltip ("Where the first tile should be placed at")]
    public Vector3 startPoint = new Vector3 (0, 0, -5);
    [Tooltip ("How many tiles should we create in advance")]
    [Range (1, 15)]
    public int initSpawnNum = 10;
    [Tooltip ("How many tiles to spawn initially with no obstacles")]
    public int initNoObstacles = 4;

    [Tooltip ("Contoll waypoint for AI bots")]
    public WaypointController WaypointController;

    /// <summary>
    /// Where the next tile should be spawned at.
    /// </summary>
    private Vector3 nextTileLocation;
    /// <summary>
    /// How should the next tile be rotated?
    /// </summary>
    private Quaternion nextTileRotation;
    /// <summary>
    /// Used for initialization
    /// </summary>
    void Start ()
    {
        //DontDestroyOnLoad(this);

        // Set our starting point
        nextTileLocation = startPoint;
        nextTileRotation = Quaternion.identity;
        for (int i = 0; i < initSpawnNum; ++i)
        {
            SpawnNextTile (i >= initNoObstacles, false);
        }
    }
public void StartGame(){
        NotRun = false;
    }

    public static bool NotRun = true;

    public void MainMenu()
    {
        SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
        NotRun = true;
    }



    /// <summary>
    /// Will spawn a tile at a certain location and setup the next position
    /// </summary>
    public void SpawnNextTile (bool spawnObstacles = true, bool isDestroy = true)
    {
        var newTile = Instantiate (tile, nextTileLocation,
            nextTileRotation);
        // Figure out where and at what rotation we should spawn
        // the next item
        var nextTile = newTile.Find ("Next Spawn Point");
        nextTileLocation = nextTile.position;
        nextTileRotation = nextTile.rotation;
        
        newTile.Find("Tile End").GetComponent<TileEndBehaviour>().IsDestroy = isDestroy;

        if (!spawnObstacles)
            return;
        // Now we need to get all of the possible places to spawn the
        // obstacle
        var obstacleSpawnPoints = new List<GameObject> ();
        var waypointSpawn = new List<GameObject> ();
        // Go through each of the child game objects in our tile
        foreach (Transform child in newTile)
        {
            // If it has the ObstacleSpawn tag
            if (child.CompareTag ("ObstacleSpawn"))
            {
                // We add it as a possibilty
                obstacleSpawnPoints.Add (child.gameObject);
            }

            if (child.CompareTag ("WaypointAI"))
            {
                waypointSpawn.Add (child.gameObject);
            }
        }
        // Make sure there is at least one
        if (obstacleSpawnPoints.Count > 0)
        {
            var ind = Random.Range (0,
                obstacleSpawnPoints.Count);
            // Get a random object from the ones we have
            var spawnPoint = obstacleSpawnPoints[ind];
            // Store its position for us to use
            var spawnPos = spawnPoint.transform.position;
            // Create our obstacle
            var newObstacle = Instantiate (obstacle, spawnPos,
                Quaternion.identity);
            // Have it parented to the tile
            newObstacle.SetParent (spawnPoint.transform);

            obstacleSpawnPoints.RemoveAt (ind);
            waypointSpawn = obstacleSpawnPoints;
        }

        WaypointAttach (waypointSpawn);

        var p = waypointSpawn[Random.Range (0,
            waypointSpawn.Count)];
    }

    public GameObject Player;

    public NavMeshSurface Nav;

    public void WaypointAttach (List<GameObject> waypointSpawn)
    {
        if (waypointSpawn.Count <= 0)
        {
            return;
        }

        var spawnPoint = waypointSpawn[Random.Range (0,
            waypointSpawn.Count)];

        WaypointController.AttachNewWaypoint (spawnPoint);
    }

    public void LoadLevel(string level)
    {
        SceneManager.LoadScene(level);
    }
}