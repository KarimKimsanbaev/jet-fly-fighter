﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Aeroplane;
/// <summary>
/// Handles spawning a new tile and destroying this one
/// upon the player reaching the end
/// </summary>
public class TileEndBehaviour : MonoBehaviour
{
    [Tooltip ("How much time to wait before destroying " +
        "the tile after reaching the end")]
    public float destroyTime = 1.5f;
    void OnTriggerEnter (Collider col)
    {
        // First check if we collided with the player
        if (col.gameObject.name == "CollaiderPanel")
        {
            // If we did, spawn a new tile
            GameObject.FindObjectOfType<GameController> ().SpawnNextTile ();
            if(IsDestroy)
            {
                // And destroy this entire tile after a short delay
                Destroy (transform.parent.gameObject, destroyTime);
            }
        }
    }

    public bool IsDestroy = true;
}