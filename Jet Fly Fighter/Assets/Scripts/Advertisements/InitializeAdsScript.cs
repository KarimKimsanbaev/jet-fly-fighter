﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

public class InitializeAdsScript : MonoBehaviour, IUnityAdsListener, IDisposable
{
    string gameId = "4020057";

    // Start is called before the first frame update
    void Start ()
    {
        Advertisement.AddListener (this);

        // Initialize the Ads service:
        Advertisement.Initialize (gameId);
    }

    public void ShowInterstitialAd (Action<ShowResult> callback = null)
    {
        var attemptAdsCount = PlayerPrefs.GetInt ("AttemptAdsCount");
        var canShowAds = attemptAdsCount % 4 == 0;

        // Check if UnityAds ready before calling Show method:
        if (canShowAds)
        {
            if (Advertisement.IsReady())
            {
                PlayerPrefs.SetInt("AttemptAdsCount", attemptAdsCount + 1);
                Advertisement.Show(
                    new ShowOptions()
                    {
                        resultCallback = callback
                    }
                );
            }
            else
            {
                Debug.Log("Interstitial ad not ready at the moment! Please try again later!");
                StartCoroutine(ReadyAdObserver());
            }
        }
        else{
             PlayerPrefs.SetInt("AttemptAdsCount", attemptAdsCount + 1);
            SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
        }
    }

    IEnumerator ReadyAdObserver ()
    {
        while (!Advertisement.IsReady ())
        {
            Debug.Log ($"Ad is Not Ready");
            yield return new WaitForSeconds (1f);
        }

        StopAllCoroutines ();
    }

    public void OnUnityAdsReady (string placementId)
    {
        Debug.Log ($"Ready Ad: {placementId}");
        // If the ready Ad Unit or legacy Placement is rewarded, show the ad:
        if (placementId == gameId)
        {
            // Optional actions to take when theAd Unit or legacy Placement becomes ready (for example, enable the rewarded ads button)
        }
    }

    public void OnUnityAdsDidError (string message)
    {
        Debug.Log ($"Ad Error: {message}");
        // Log the error.
    }

    public void OnUnityAdsDidStart (string placementId)
    {
        Debug.Log ($"Ad Start: {placementId}");
        // Optional actions to take when the end-users triggers an ad.
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish (string placementId, ShowResult showResult)
    {
        // TODO: Use this concept for make custom debug logger.
        Debug.Log ($"[{this.GetType().Name}] {placementId} {showResult.ToString ()}");

        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            // Reward the user for watching the ad to completion.
        }
        else if (showResult == ShowResult.Skipped)
        {
            // Do not reward the user for skipping the ad.
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning ("The ad did not finish due to an error.");
        }
    }

    public void Dispose ()
    {
        Advertisement.RemoveListener (this);
    }
}