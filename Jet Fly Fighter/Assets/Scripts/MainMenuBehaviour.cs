﻿using UnityEngine;

public class MainMenuBehaviour : MonoBehaviour
    {
        public InitializeAdsScript Ads;

        /// <summary>
        /// Will load a new scene upon being called
        /// </summary>
        /// <param name="levelName">The name of the level we want
        /// to go to</param>
        public void LoadLevel(string levelName)
        {
        //SceneManager.LoadScene (levelName);

        //PlayerBehaviour.Instance.GetComponent<AeroplaneController>();

#if UNITY_ADS
        // if (UnityAdController.showAds)
        // {
        //     // Show an ad
        //     UnityAdController.ShowAd ();
        // }
#endif
    }

#if UNITY_IAP
    public void DisableAds ()
    {
        UnityAdController.showAds = false;
        // Used to store that we shouldn't show ads
        PlayerPrefs.SetInt ("Show Ads", 0);
    }
#endif

        virtual protected void Start()
        {
#if UNITY_IAP
        // Initialize the showAds variable
        UnityAdController.showAds = (PlayerPrefs.GetInt ("Show Ads", 1) == 1);
#endif
        }
    }