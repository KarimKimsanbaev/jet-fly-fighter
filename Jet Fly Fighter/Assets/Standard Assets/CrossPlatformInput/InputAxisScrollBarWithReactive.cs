﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets.CrossPlatformInput
{
    public class InputAxisScrollBarWithReactive : InputAxisScrollbar
    {
        public Scrollbar scrollbar;

        void Update ()
        {
            var notRaw = CrossPlatformInputManager.GetAxis (axis);
            var raw = CrossPlatformInputManager.GetAxisRaw (axis);
            scrollbar.value = raw + 0.5f; //Range(0.0f,1.0f)
        }
    }
}