﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //iTween.RotateTo(gameObject, new Vector3(0, 180, 0), 5);
    }

    public GameObject target;

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround (target.transform.position, Vector3.up , 10 * Time.deltaTime ); 
    }
}
