﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeOnDestroy : MonoBehaviour, IDisposable
{

    public PlayerBehaviour player;
    private bool disposedValue;

    // Start is called before the first frame update
    void Start ()
    {
        if (!player)
        {
            player = GameObject.FindObjectOfType<PlayerBehaviour> ();
        }

        player.PlayerObstacleCollision += OnPlayerObstacleCollision;
    }

    private void OnPlayerObstacleCollision (object sender, EventArgs e)
    {
        iTween.ShakePosition (gameObject, Vector3.up, 1f);
    }

    public void Dispose ()
    {
        player.PlayerObstacleCollision -= OnPlayerObstacleCollision;
    }
}